"""
CSS 475: Database Project
Author: Leo Liu

This program acts as a very simple Library Database UI. A user can 
    1. Checkout an item 
    2. Return an item 
    3. View the inventory of all library systems
    4. View the late items currently checked out
    5. Add new items to the library catalogue
    6. Remove items from the library catalogue 

To run the program, use a terminal or command prompt. 
"""

import sqlite3
import sys
conn = sqlite3.connect('..\CSS475Library.db')

c = conn.cursor()

# This method allows a librarian to check out an item from the system for a 
# patron. It creates a new record in the CHECKOUT table and decreases the 
# corresponding inventory count. 
def checkOut():
    print ("ITEM CHECKOUT FORM")
    Patron_ID = input("Patron ID: ")
    Book_ID = input("Book ID: ")
    Library_ID = input("Library ID: ")
    Checkout_date = input("Checkout Date: ")
    Due_date = input("Due Date: ")
    
    checkOutData = [Patron_ID, Book_ID, Library_ID, Checkout_date, Due_date]
    with conn:
        c.execute('INSERT INTO CHECKOUT(Patron_ID, Book_ID, Library_ID, \
                    Checkout_Date, Due_Date) values(?,?,?,?,?)', checkOutData)
           
        c.execute('UPDATE INVENTORY SET Quantity = Quantity - 1 \
                    WHERE Book_ID = ? AND Library_ID = ?', (Book_ID,Library_ID))
    user_in = input("Item Checked Out. Enter ANY KEY to return to the main menu.")

# This method allows a librarian to complete a library item transaction for
# an item being returned. 
def returnItems():
    print("ITEM RETURN FORM")
    Patron_ID = input("Patron ID: ")
    Book_ID = input("Book ID: ")
    Library_ID = input("Library ID: ")
    with conn:
        c.execute('DELETE FROM CHECKOUT WHERE Patron_ID = ? AND Book_ID = ? \
                    AND Library_ID = ?',(Patron_ID, Book_ID, Library_ID))
        c.execute('UPDATE INVENTORY SET Quantity = Quantity + 1 \
                    WHERE Book_ID = ? AND Library_ID = ?',(Book_ID, Library_ID))
    user_in = input("Item Returned. Enter ANY KEY to return to the main menu.")
    
# This method allows the user to see what items are in the inventory of a
# library. 
def inventory():
    print("Retrieving Inventory...")
    with conn:
        for row in c.execute("SELECT LIBRARY_BRANCH.Library_Name, \
                                BOOK.Book_title, INVENTORY.Quantity \
                            FROM INVENTORY, BOOK, LIBRARY_BRANCH \
                            WHERE INVENTORY.Book_ID = BOOK.Book_ID\
                            AND LIBRARY_BRANCH.Library_ID = INVENTORY.Library_ID"):
            print(row)
    user_in = input("Enter ANY KEY to return to the main menu.")
        
    
# This method allows the user to see who has items checked out past the due
# date.
def lateItems():
    print("Retrieving Late Items...")
    with conn:
        for row in c.execute("SELECT BOOK.book_title \
                            FROM BOOK, CHECKOUT \
                            WHERE BOOK.Book_ID = CHECKOUT.Book_ID \
                            AND CHECKOUT.Due_Date > CURRENT_DATE;"): 
                            print(row)
    user_in = input("Enter ANY KEY to return to the main menu.")

# This method prompts the user to input information for the item they are
# adding to the catalogue.
def addItems():
    print("ITEM ENTRY FORM")
    bookTitle = input("Book Title: ")
    ISBN = input("ISBN: ")
    Author_ID = input("Author_ID: ")
    Genre = input("Genre: ")
    Dew_Dec = input("Dewey Decimal: ")

    bookData = [bookTitle, ISBN, Author_ID, Genre, Dew_Dec]
    with conn:
        c.execute('INSERT INTO BOOK(Book_title, ISBN, Author_ID, Genre, \
                Dew_Dec) VALUES (?,?,?,?,?)', bookData)
    user_in = input("Entry Added. Enter ANY KEY to return to the main menu.")
    
# This method prompts the user to identify an item to remove from the library
# catalogue. 
def removeItems():
    print("ITEM DELETION FORM")
    dewDec = input("Dewey Decimal: ")
    with conn:
        c.execute('DELETE FROM BOOK WHERE Book.Dew_Dec = ?', (dewDec,))
    user_in = input("Entry Deleted. Enter ANY KEY to return to the main menu.")
    


while(True):
    print("""
>>> WELCOME TO THE LIBRARY SYSTEM <<<

Enter a number to select an option:
--------------------------------- 
1. Check an item out
2. Return an item
3. Retrieve inventory information
4. List all of the late items in the library
5. Add an item not existing in the library
6. Remove an item already existing in the library
7. Quit.
""")

    user_in = input() 
    
    if user_in == '1':
        checkOut();
    elif user_in == '2':
        returnItems();
    elif user_in == '3':
        inventory();
    elif user_in == '4':
        lateItems();
    elif user_in == '5':
        addItems();
    elif user_in == '6':
        removeItems();
    elif user_in == '7':
        sys.exit();
    else:
        print('Error: Unknown Command. Please try again');
    
